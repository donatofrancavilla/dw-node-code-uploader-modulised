# Demandware Node Server Upload

This is a small utility tool which allows to upload single cartridge files or a cartridge as a whole to a configured Demandware server.

# Installation

Clone this repository and run 'npm install' in the root directory of your local working copy. The credentials for Demandware server need to be passed as arguments. 

```
Usage: node ./lib/main <file> <server host name> <code version> <username> <password>.
```

# Release history

- 2015/07/06 - [0.0.1]
    - initial version

# Support / Contributing

Please feel free to create issues and enhancement requests or discuss on the existing ones, this will help us understanding in which area the biggest need is. For discussions please start a topic on the [Community Suite discussion board](https://xchange.demandware.com/community/developer/community-suite/content).

# License

This plugin is provided under MIT license.