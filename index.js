var Server = require('./lib/server');
var Resource = require('./lib/resource');
var util = require('util');
var colors = require('colors');

module.exports = function(params) {
  /*
    defaults = {
      filePath    : "",
      host        : "",
      version     : "",
      username    : "",
      password    : "",
      method      : ""
    }
  */

  var resource = new Resource(params.filePath);
  var server   = new Server(params.host, params.username, params.password, params.version);
  server.upload(
      resource,
      function(method) {
        util.log("Upload sucessfully finished.".green);
      },
      function(e) {
        console.error(colors.red("Upload Failed "+ resource.getLocalPath()));
      },
      false);
/*
  console.log('Resource isFile ', resource.isFile());
  console.log('Resource isDeleted ', resource.isDeletedResource());
  console.log('Resource isFolder ', resource.isFolder());
  console.log('Resource isInCartridge ', resource.isInCartridge());
  */

}