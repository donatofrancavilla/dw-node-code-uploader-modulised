var path = require('path');
var fs = require('fs');
var zlib = require('zlib');


var ResourceObject = function (localPath){

    this.localPath = localPath;
    this.inCartridge = false;
    this.resourceIsFolder = fs.lstatSync(localPath).isDirectory();
    this.resourceIsFile = !fs.lstatSync(localPath).isDirectory();
    this.isDeleted = false;

    var baseDir = localPath;
    this.cartridgeName = '';
    while(baseDir.length > 1){
        var currentDirectoryName = path.basename(baseDir);
        
        baseDir = path.dirname(baseDir);
        
        if(currentDirectoryName == 'cartridge'){
            this.inCartridge = true;
            this.cartridgeName = path.basename(baseDir);
            baseDir = path.dirname(baseDir);
            
            break;
        }
        
    }
    
    if(this.cartridgeName == '' && this.isCartridge()){
        // this means the path is a cartridge directory
        // use directory name as cartridge name
        this.cartridgeName = path.basename(localPath);
        // ...and its parent as baseDir
        baseDir = path.dirname(localPath);
    }
    
    if(this.cartridgeName == ''){
        baseDir = path.dirname(localPath);
        if(baseDir.length > 1 && new ResourceObject(baseDir).isCartridge()){
            // this means the path is a file next to the 'cartridge' directory
            this.inCartridge = true;
            this.cartridgeName = path.basename(baseDir);
            baseDir = path.dirname(baseDir);
        }
    }
    
    this.baseDir  = baseDir;
    this.codePath = path.relative(baseDir, localPath);
    
    if (!fs.existsSync(this.localPath)) {
            this.isDeleted = true;
            return false;
    }

    if(this.isCartridge()){
        // add a '.zip' for the upload location
        this.codePath += '.zip';
    }

    //console.log('cartridgeName ' + this.cartridgeName);
    //console.log('codePath ' + this.codePath);
};

ResourceObject.prototype = {
    /**
     * Returns true in case the given resource in inside of a cartridge
     */
    isInCartridge : function(){
        return this.inCartridge;
    },
    /**
     * Returns true in case the given resource is a cartridge
     */
    isCartridge : function(){
        
        if(this.isFile() || this.isDeletedResource()){
            return false;
        }
        
        
        var fileNames = fs.readdirSync(this.localPath);
        
        for (var index in fileNames){
          
             if(fileNames[index] == 'cartridge'){
                 return true;
             }
        }
        
        return false;
    },
    isDeletedResource : function() {
        return this.isDeleted;
    },
    /**
     * Return the cartridge name of the cartridge the resource is in
     * 
     * @returns the cartridge name
     */
    getCartridgeName : function(){
        return this.cartridgeName;
    },
    /**
     * Return the resource path on the Demandware server
     * 
     * @returns the resource path on the Demandware server
     */
    getCodePath : function(){
        return this.codePath;
    },
    /**
     * Return the resource folder on the Demandware server
     * 
     * @returns the resource folder on the Demandware server
     */
     getCodeFolder : function(){
        if(this.isCartridge()){
            return "";
        }
        return path.dirname(this.codePath);
    },
    isFile : function(){
        return this.resourceIsFile;
    },
    isFolder : function(){
        return this.resourceIsFolder;
    },
    /**
     * Return the local resource path
     * 
     * @returns the local resource
     */
    getLocalPath : function(){
        return this.localPath;
    },
    /**
     * Return the resource content, for cartridges the zipped cartridge is returned
     * 
     * @returns the resource file content
     */
    getFileContent : function(callback){
        if(this.isCartridge()){
            var archiver = require('archiver');

            var zipFileName = this.baseDir + path.sep + this.codePath;
            var zipData = Buffer(0);
            var archive = archiver('zip');
            
            archive.on('data', function (chunk) {
                zipData = Buffer.concat([zipData, chunk]);
            });
                
            archive.on('end', function () {
                console.log(archive.pointer() + ' bytes zipped');
                callback(zipData);
            });
            
            archive.on('error', function(err){
                console.error('Error while zipping cartridge '+err)
                throw err;
            });
            
            archive.directory(this.getLocalPath(),this.getCartridgeName()).finalize();
        }else if(this.isDeletedResource()) {
            callback();
        } else if(this.isFolder()) {
            callback(fs.readdirSync(this.getLocalPath()));
        }else {
            callback(fs.readFileSync(this.getLocalPath()));
        }
    }
};

module.exports = ResourceObject;