var q = require("q");
var filewalker = require("filewalker");
var fs = require("fs");
var Resource = require('./resource');

var CARTRIDGES = "/on/demandware.servlet/webdav/Sites/Cartridges/";
/**
 *  @class
 *  Helper class holding a status of recursive to folder trail
 */
var FolderTree = function (codeFolder) {
    this.finalFolder = codeFolder;
    this.folderArray = codeFolder.split("/");
    this.activeFolder = "/";
    this.activeIndex = 0;
};
/**
 *  activates child folder
 */
FolderTree.prototype.nextFolder = function() {
    if (this.finalFolder == "" || this.activeIndex === this.folderArray.length) {
        return false;
    } else {
        this.activeFolder = this.activeFolder + this.folderArray[this.activeIndex] + "/";
        this.activeIndex++;
        return this.activeFolder;
    }
}; 

/**
 *  @class
 *  class handles communication towards demandware instance 
 */
var Server = function (host, user, password, version) {
    this.codePath = CARTRIDGES + version;
    this.host = host;
    this.user = user;
    this.password = password;
};


/**
 *  uploads file to demandware server and ensure that any prerequisites are solved beforehand
 */
Server.prototype.upload = function (resource, success, error, skipDirectoryLookup) {
    var serverPath = this.codePath;
    var server = this;
    var fileReadSuccess = function(content) {
        if (skipDirectoryLookup) {
            server.ensureDirectories(resource, function() {
                var path = serverPath + "/" + resource.getCodePath();
                if(resource.isDeletedResource()) {
                    server.executeHttpRequest("DELETE", path, {
                    'onSuccess': function() {//console.log('yes deleted')
                }, 
                    'onError' : function() {//console.log('no deleted')
                }});
                } else if(resource.isFolder()) {
                    server.executeHttpRequest("MKCOL", path, {
                    'onSuccess': function() {//console.log('yes folder')
                }, 
                    'onError' : function() {//console.log('no folder')
                }});
                }else {
                    server.executeHttpRequest(method, path, {
                        'onSuccess': success, 
                        'onError' : error,
                        'requestBody' : content});
                }
            });
        } else {
            var path = serverPath + "/" + resource.getCodePath();
                if(resource.isDeletedResource()) {
                    server.executeHttpRequest("DELETE", path, {
                    'onSuccess': success, 
                    'onError' : error});
                } else if(resource.isFolder()) {
                    server.addFolder(resource, success, error);
                    server.executeHttpRequest("MKCOL", path, {
                    'onSuccess': success, 
                    'onError' : error});
                }else {
                    server.ensureDirectories(resource, function() {
                        server.executeHttpRequest('PUT', path, {
                            'onSuccess': success, 
                            'onError' : error,
                            'requestBody' : content});
                    });
                }
               
        }
    }
    resource.getFileContent(fileReadSuccess);
};

Server.prototype.addFolder = function(resource, success, error) {
    var server = this;
    filewalker(resource.getLocalPath())
      .on('dir', function(p) {
        server.upload(new Resource(resource.getLocalPath() + '/' + p), success, error, false);
      })
      .on('file', function(p, s) {
        server.upload(new Resource(resource.getLocalPath() + '/' + p), success, error, false);
      })
      .walk();
}

/**
 *  uploads file to demandware server and ensure that any prerequisites are solved beforehand
 */
Server.prototype.uploadCartridge = function (resource, success, error) {
    var serverPath = this.codePath + "/" + resource.codePath;
    if (serverPath.indexOf('.zip') != -1) {
        var onUploadSuccess = function () {
            this.executeHttpRequest("POST", serverPath, {
                'onSuccess': success, 
                'onSuccessArgs' : [resource, success, error],
                'onError' : error,
                'requestBody' : 'method=UNZIP'
            });
        };
        this.upload(resource, onUploadSuccess, error, true);
    } else {
        error.apply(this,["No zipfile given"]);
    }
};

/**
 *  checks if folders need to be created or not. All folders are created before a file can be uploaded
 */
Server.prototype.ensureDirectories = function (resource, finish) {
    this.executeHttpRequest("GET", this.codePath + "/" + resource.getCodePath(), {
        'onSuccess' : finish,  
        'onError' : function(){this.createFolderStructure(resource, finish)}});
};

/**
 *  will create folders structure to ensure file can be uploaded to it designated folder
 */
Server.prototype.createFolderStructure = function (resource,success) {
    var folderTree = new FolderTree(resource.getCodeFolder());
    var firstFolder = folderTree.nextFolder();
    this.createPathElement(folderTree,firstFolder,success);
}

/**
 *  create a single folder and calls the next folder creation method
 */
Server.prototype.createPathElement = function (tree, currentFolder, callBack) {
    var server = this;
    if (currentFolder) {
        var serverFolderPath = this.codePath + currentFolder;
     
        this.executeHttpRequest("MKCOL", serverFolderPath, {
            'onSuccess' : function(tree,lastFolder, callBack){
                var nextFolder = tree.nextFolder();
                server.createPathElement(tree,nextFolder, callBack);
            },
            'onSuccessArgs' : [tree, currentFolder, callBack], 
            'onError' : function(tree,lastFolder, callBack){
                var nextFolder = tree.nextFolder();
                server.createPathElement(tree,nextFolder, callBack);
            }, 
            'errorHanderArgs' : [tree, currentFolder, callBack]
        });

    } else {
        callBack.apply(this);
    }
}

/**
 *  wrapper for all http requests to cartridges folder
 */
Server.prototype.executeHttpRequest = function (method, path, options) {
    //console.info(method + "-" + path);
    var debugArguments = JSON.stringify(arguments);
    var success = options.onSuccess ? options.onSuccess : function() {};
    var successArgs = options.onSuccessArgs ? options.onSuccessArgs : [];
    var error = options.onError ? options.onError : function() {};
    var errorArgs = options.errorHanderArgs ? options.errorHanderArgs : [];
    var requestBody = options.requestBody;
    
    var server = this;
    var request = require('https').request({
        hostname: this.host,
        path: path,
        method: method,
        port : 443,
        auth: (this.user+':'+this.password),
        headers : {
            'User-Agent' : 'Danny\'s node upload script',
            'Content-Type' : "application/x-www-form-urlencoded"
        }
    },function(response) {
        response.setEncoding('utf8');

        response.on('data', function (chunk) {

        });
        
        response.on('error', function(e) {
            console.error(e);
            error.apply(server,errorArgs);
        });
      
        response.on('end', function (chunk) {
            if (response.statusCode >= 200 && response.statusCode < 300) {
                success.apply(server, successArgs);
            } else {                            
                //console.info('HTTP Request failed with status code ' + response.statusCode);
                error.apply(server, errorArgs);
            }
        });
    });
    
    request.on('error', function(e) {
        console.error("request error" + e);
        error.apply(server, errorArgs);
    });
    if (requestBody) {
        if (method != 'PUT') {
            //console.info('request Body '  + requestBody );
        }
        request.write(requestBody);
    }
    request.end();
}

Server.prototype.toString = function() {
    return "DemandwareServerConnection: " + this.host;
}

module.exports = Server;